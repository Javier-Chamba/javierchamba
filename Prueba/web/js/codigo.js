/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function (window, document, JSON) {
    var i = 0;
    'use strict';

    var url = 'ws://' + window.location.host + '/Prueba/jugar',
            ws = new WebSocket(url),
            mensajes = document.getElementById('conversacion'),
            btnUno = document.getElementById('btnUno'),
            btnDos = document.getElementById('btnDos'),
            btnTres = document.getElementById('btnTres'),
            btnCuatro = document.getElementById('btnCuatro'),
            btnCinco = document.getElementById('btnCinco'),
            btnSeis = document.getElementById('btnSeis'),
            btnSiete = document.getElementById('btnSiete'),
            btnOcho = document.getElementById('btnOcho'),
            btnNueve = document.getElementById('btnNueve');

    ws.onopen = onOpen;
    ws.onclose = onClose;
    ws.onmessage = onMessage;

    btnUno.addEventListener('click', enviarUno);
    btnDos.addEventListener('click', enviarDos);
    btnTres.addEventListener('click', enviarTres);
    btnCuatro.addEventListener('click', enviarCuatro);
    btnCinco.addEventListener('click', enviarCinco);
    btnSeis.addEventListener('click', enviarSeis);
    btnSiete.addEventListener('click', enviarSiete);
    btnOcho.addEventListener('click', enviarOcho);
    btnNueve.addEventListener('click', enviarNueve);

    function onOpen() {
        console.log('Conectado...');
    }

    function onClose() {
        console.log('Desconectado...');
    }

    function enviarUno() {
        ws.send(JSON.stringify(enviar('1')));
    }
    
    function enviarDos() {
        ws.send(JSON.stringify(enviar('2')));
    }
    
    function enviarTres() {
        ws.send(JSON.stringify(enviar('3')));
    }
    
    function enviarCuatro() {
        ws.send(JSON.stringify(enviar('4')));
    }
    
    function enviarCinco() {
        ws.send(JSON.stringify(enviar('5')));
    }
    
    function enviarSeis() {
        ws.send(JSON.stringify(enviar('6')));
    }
    
    function enviarSiete() {
        ws.send(JSON.stringify(enviar('7')));
    }
    
    function enviarOcho() {
        ws.send(JSON.stringify(enviar('8')));
    }
    
    function enviarNueve() {
        ws.send(JSON.stringify(enviar('9')));
    }

    function onMessage(evt) {
        var obj = JSON.parse(evt.data);
                //msg = 'Posicion: ' + obj.posicion + ' estado: ' + obj.estado;
        if (obj.posicion === '1') {
            btnUno.value = obj.estado;
            btnUno.disabled = true;
            mensajes.innerHTML = comprobar(obj.estado);
            //console.log(comprobar(obj.estado));
        }else if(obj.posicion === '2') {
            btnDos.value = obj.estado;
            btnDos.disabled = true;
            mensajes.innerHTML = comprobar(obj.estado);
            //console.log(comprobar(obj.estado));
        }else if(obj.posicion === '3') {
            btnTres.value = obj.estado;
            btnTres.disabled = true;
            mensajes.innerHTML = comprobar(obj.estado);
            //console.log(comprobar(obj.estado));
        }else if(obj.posicion === '4') {
            btnCuatro.value = obj.estado;
            btnCuatro.disabled = true;
            mensajes.innerHTML = comprobar(obj.estado);
        }else if(obj.posicion === '5') {
            btnCinco.value = obj.estado;
            btnCinco.disabled = true;
            mensajes.innerHTML = comprobar(obj.estado);
        }else if(obj.posicion === '6') {
            btnSeis.value = obj.estado;
            btnSeis.disabled = true;
            mensajes.innerHTML = comprobar(obj.estado);
        }else if(obj.posicion === '7') {
            btnSiete.value = obj.estado;
            btnSiete.disabled = true;
            mensajes.innerHTML = comprobar(obj.estado);
        }else if(obj.posicion === '8') {
            btnOcho.value = obj.estado;
            btnOcho.disabled = true;
            mensajes.innerHTML = comprobar(obj.estado);
        }else{
            btnNueve.value = obj.estado;
            btnNueve.disabled = true;
            mensajes.innerHTML = comprobar(obj.estado);
        }
        
        i += 1;
//        mensajes.innerHTML += '<br/>' + msg;
    }
    
    function marcar(){
        var aux;
        if ((i + 1) % 2 !== 0)
            aux = 'x';
        else
            aux = '0';
        return aux;
    }
    
    
    function enviar(numero){
        var msg = {
            posicion: numero,
            estado: marcar()
        };
        return msg;
    }
    
    function comprobar(marcador){
        var mensaje;
        if (btnUno.value===marcador && btnDos.value===marcador && btnTres.value===marcador) //Primera fila
            mensaje=marcador;
        else if(btnCuatro.value===marcador && btnCinco.value===marcador && btnSeis.value===marcador) //Segunda fila
            mensaje=marcador;
        else if(btnSiete.value===marcador && btnOcho.value===marcador && btnNueve.value===marcador) //Tercera fila
            mensaje=marcador;
        else if(btnUno.value===marcador && btnCuatro.value===marcador && btnSiete.value===marcador) //Primera conlumna
            mensaje=marcador;
        else if(btnDos.value===marcador && btnCinco.value===marcador && btnOcho.value===marcador) //Segunda conlumna
            mensaje=marcador;
        else if(btnTres.value===marcador && btnSeis.value===marcador && btnNueve.value===marcador) //Tercera conlumna
            mensaje=marcador;
        else if(btnSiete.value===marcador && btnCinco.value===marcador && btnTres.value===marcador) //Diagonal principal
            mensaje=marcador;
        else if(btnUno.value===marcador && btnCinco.value===marcador && btnNueve.value===marcador) //Diagonal secundaria
            mensaje=marcador;
        else
            mensaje='';
        if (mensaje==='x'){
            mensaje='Ganador x...!';
            document.getElementById('TresRayas').style.pointerEvents = "none"; //Desactivar el div de TresRayas
            //document.getElementById('TresRayas').style.pointerEvents = "auto"; //Activar el div de TresRayas
        }else if (mensaje==='0'){
            mensaje='Ganador 0...!';
            document.getElementById('TresRayas').style.pointerEvents = "none"; //Desactivar el div de TresRayas
            //document.getElementById('TresRayas').style.pointerEvents = "auto"; //Desactivar el div de TresRayas
        }
        return mensaje;
    }
    
    

})(window, document, JSON);
