/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.io.IOException;
import java.io.Reader;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author Javier
 */
public class DecoderMarcado implements Decoder.TextStream<Marcado>{

    @Override
    public Marcado decode(Reader reader) throws DecodeException, IOException {
         Marcado marcado=new Marcado();
        try(JsonReader jsonReader = Json.createReader(reader)){
            JsonObject json = jsonReader.readObject();
            marcado.setPosicion(json.getString("posicion"));
            marcado.setEstado(json.getString("estado"));
        }
        return marcado;
    }

    @Override
    public void init(EndpointConfig config) {
        
    }

    @Override
    public void destroy() {
        
    }
    
}
