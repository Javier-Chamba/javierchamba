/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.io.IOException;
import java.io.Writer;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author Javier
 */
public class EncoderMarcado  implements Encoder.TextStream<Marcado>{

    @Override
    public void encode(Marcado object, Writer write) throws EncodeException, IOException {
            JsonObject json = Json.createObjectBuilder()
                .add("posicion", object.getPosicion())
                .add("estado", object.getEstado())
                .build();
        try(JsonWriter jsonWrite = Json.createWriter(write)) {
            jsonWrite.writeObject(json);
        }
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
    
}
