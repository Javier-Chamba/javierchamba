/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import objetos.DecoderMarcado;
import objetos.EncoderMarcado;
import objetos.Marcado;

/**
 *
 * @author Javier
 */

@ServerEndpoint(value = "/jugar",encoders = {EncoderMarcado.class},
        decoders = {DecoderMarcado.class})
public class Jugar {
    
   private static final List<Session> conectados = new ArrayList<>();
    
    @OnOpen
    public void inicio(Session sesion){
        conectados.add(sesion);
    }
    
    @OnClose
    public void salir(Session sesion){
        conectados.remove(sesion);
    }
    
    @OnMessage
    public void mensaje(Marcado marcado) throws IOException, EncodeException {
        for(Session sesion : conectados){
            sesion.getBasicRemote().sendObject(marcado);
        }
    }
    
    
}
